import { agent } from './veramo/setup.js'

async function main() {
  const identifier = await agent.didManagerGet({ did: 'did:ethr:0x2851e010738422CE8786D9F86e166Fc6E1030a1a' })

  const verifiableCredential = await agent.createVerifiableCredential({
    credential: {
      issuer: { id: identifier.did },
      credentialSubject: {
        id: 'did:ethr:0x7b0D35490f5245b93b5bd832F71e6a32055cFaaC',
        role: 'operator',
      },
    },
    proofFormat: 'EthereumEip712Signature2021',
  })
  console.log(`New credential created`)
  console.log(JSON.stringify(verifiableCredential, null, 2))
}

main().catch(console.log)