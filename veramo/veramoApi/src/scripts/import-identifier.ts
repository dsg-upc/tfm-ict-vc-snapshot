import { TKeyType } from '@veramo/core'
import { agent } from './veramo/setup.js'

async function main() {
  const key = {
    kms:"local",
    privateKeyHex: "0xdb7bbaee5f30c525a3854958231fe89f0cdbeec09479c769e3d3364f0e666d6a",
    type: 'Secp256k1' as TKeyType,
    publicKeyHex: '0x2851e010738422CE8786D9F86e166Fc6E1030a1a'
  }

  const identifier = await agent.didManagerImport({
    keys: [key],
    provider: "did:ethr",
    did: "did:ethr:0x2851e010738422CE8786D9F86e166Fc6E1030a1a"
  })
  console.log(`New identifier created`)
  console.log(JSON.stringify(identifier, null, 2))
}

main().catch(console.log)