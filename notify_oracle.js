const axios = require('axios')

axios.post('http://127.0.0.1:3010/oracle', {
    api_token: "YB33NQCikdGtWjd.xJo1RT8fNGZkSZo0s8WlnAPPLQSiC1eu1KL6cc9GZognVEZl6VURdLpHkpkL5KEX",
    Credential: {
        "issuer": {
          "id": "did:ethr:0x2851e010738422CE8786D9F86e166Fc6E1030a1a"
        },
        "credentialSubject": {
          "id": "did:ethr:0x7b0D35490f5245b93b5bd832F71e6a32055cFaaC",
          "role": "operatorr"
        },
        "issuanceDate": "2024-01-16T18:01:53.134Z",
        "@context": [
          "https://www.w3.org/2018/credentials/v1"
        ],
        "type": [
          "VerifiableCredential"
        ],
        "proof": {
          "verificationMethod": "did:ethr:0x2851e010738422CE8786D9F86e166Fc6E1030a1a#controller",
          "created": "2024-01-16T18:01:53.134Z",
          "proofPurpose": "assertionMethod",
          "type": "EthereumEip712Signature2021",
          "proofValue": "0xf8c6e1113e56ea80c1edef50ffbc871dd5c413881e0f72879d15bec804c27d15493664c7a7ca9f3142c42ea03be94f55f70c5c06ec07732708b860775fca16ed1c",
          "eip712": {
            "domain": {
              "chainId": 457,
              "name": "VerifiableCredential",
              "version": "1"
            },
            "types": {
              "EIP712Domain": [
                {
                  "name": "name",
                  "type": "string"
                },
                {
                  "name": "version",
                  "type": "string"
                },
                {
                  "name": "chainId",
                  "type": "uint256"
                }
              ],
              "CredentialSubject": [
                {
                  "name": "id",
                  "type": "string"
                },
                {
                  "name": "role",
                  "type": "string"
                }
              ],
              "Issuer": [
                {
                  "name": "id",
                  "type": "string"
                }
              ],
              "Proof": [
                {
                  "name": "created",
                  "type": "string"
                },
                {
                  "name": "proofPurpose",
                  "type": "string"
                },
                {
                  "name": "type",
                  "type": "string"
                },
                {
                  "name": "verificationMethod",
                  "type": "string"
                }
              ],
              "VerifiableCredential": [
                {
                  "name": "@context",
                  "type": "string[]"
                },
                {
                  "name": "credentialSubject",
                  "type": "CredentialSubject"
                },
                {
                  "name": "issuanceDate",
                  "type": "string"
                },
                {
                  "name": "issuer",
                  "type": "Issuer"
                },
                {
                  "name": "proof",
                  "type": "Proof"
                },
                {
                  "name": "type",
                  "type": "string[]"
                }
              ]
            },
            "primaryType": "VerifiableCredential"
          }
        }
      },
    },
    {
        headers: {
            dlt: "ethereum"
        }
    }).then(function (response) {
        console.log(response.data);
    })
    .catch(function (error) {
        console.log(error);
    });