import React, { useState, useEffect, Fragment } from 'react';
import { Container, Row, Col, Form, Button, Tabs, Tab, Accordion, ListGroup, Spinner, ListGroupItem } from 'react-bootstrap';
import axios from 'axios';

const TrustActor = (props) => {
    const [template, setTemplate] = useState("")
    const [verifyPressed, setVerifyPressed] = useState(0)
    const [parentCredential, setParentCredential] = useState("")
    const [verifiableCredential, setVC] = useState("")
    // const root="0x2851e010738422CE8786D9F86e166Fc6E1030a1a"
    const root="0x2851e010738422CE8786D9F86e166Fc6E1030a1a"

    const press = (turn) => {
        if (turn == 1) {
            setVerifyPressed(verifyPressed + 1)
            var vc = JSON.parse(verifiableCredential)
            axios.post('http://localhost:3016/verify',
                {
                    credential: JSON.parse(verifiableCredential),
                }
            ).then((data) => {
                console.log(data)
                console.log(vc.issuer.id.slice(9))
                if (vc.issuer.id.slice(9) == root) {
                    setParentCredential(
                        <ListGroup>
                            <TrustActor type="Root" address={root} key_for_event={props.key_for_event + props.address + root} apiUrl={props.apiUrl}></TrustActor>
                        </ListGroup>
                    )
                }
                else {
                    setParentCredential(
                        <TrustActor type="Issuer" address={vc.issuer.id.slice(9)} key_for_event={props.key_for_event + props.address + "0x66525bc999a2675437eBCFBdc88Aa5E6437B9018"} apiUrl={props.apiUrl}></TrustActor>
                    )
                }
                // console.log("kek"+props.address)
                // console.log("kek"+JSON.stringify(vc.credentialSubject.id).slice(9))
                if (vc.credentialSubject.id.slice(9) != props.address) press(3)
                else press(2)
                // })
            })}
        else if (turn == 2) setVerifyPressed(2)
        else setVerifyPressed(3)
    }

    const fileProcess = () => {
        var [file] = document.querySelector("input[type=file]").files;
        const reader = new FileReader();

        reader.addEventListener(
            "load",
            () => {
                setVC(reader.result);
            },
            false,
        );

        if (file) {
            reader.readAsText(file);
        }
    }

    useEffect(() =>{
        // var actualDate = new Date(props.proof.timestamp * 1000)
        // setDate(actualDate)
        // console.log(verifyPressed)
        console.log("HI")

        if(props.type == "Root"){
            setTemplate(
                <ListGroup>
                <ListGroupItem>{root}<br></br>Credential type: {"Root"}</ListGroupItem>
            </ListGroup>
            )
        }
        else if(verifyPressed == 0){
            setTemplate(
            <ListGroup>
                <ListGroupItem>{props.address}<input type="file" onChange={() => fileProcess()}/><button onClick={() => press(1)} style={{float:'right'}}>Verify</button><br></br>Credential type: {props.type}</ListGroupItem>
            </ListGroup>
            )
        }
        else if (verifyPressed == 1){
            setTemplate(
                <ListGroup>
                    <ListGroupItem>{props.address}<Spinner style={{float:'right'}} animation="border" /><br></br>Credential type: {props.type}</ListGroupItem>
                </ListGroup>
                )
        }
        else if (verifyPressed == 2){
            setTemplate(
                <Accordion.Item eventKey={props.key_for_event}>
                    <Accordion.Header><div>{props.address}<span style={{float:'right'}}>✅</span><br></br>Credential type: {props.type}</div></Accordion.Header>
                    <Accordion.Body>{parentCredential}</Accordion.Body>
                </Accordion.Item>
                )
        }
        else{
            setTemplate(
                <ListGroup>
                    <ListGroupItem>{props.address}<span style={{float:'right'}}>❌</span><br></br>Credential type: {props.type}</ListGroupItem>
                </ListGroup>
                )
        }
        //setVerifyPressed(0)
    }, [verifyPressed, verifiableCredential])

    
    

    return(<div>
        {/* {template} */}
            {template}
    </div>
        
    )
}

export default TrustActor;